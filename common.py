import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler


def load_data(num_steps=40):
    data = np.array(pd.read_excel('data.xls'))

    x = data[:-1, 0]
    y = np.array(data[:-1, -1] / (data[:-1, -2] * 1000), dtype=np.float64)

    y_data = y[:]
    x_date = x[:]
    y = y_data[num_steps:]
    x = None
    for i in range(len(y_data) - num_steps):
        if x is None:
            x = np.array([y_data[i:i + num_steps]], dtype=np.float64)
        else:
            x = np.append(x, np.array([y_data[i:i + num_steps]]), axis=0)

    last_x_data = x[-1, :]

    return {
        'x_date': x_date,
        'X': x,
        'Y': y,
        'y_data': y_data,
        'last_x_data': last_x_data
    }

def predict_accuracy(test_data, predict_data, error_percent=0.01):
    error_val = np.mean(test_data) * error_percent
    return np.sum(np.abs(test_data - predict_data) < error_val) / test_data.size


def get_train_test_data(X, Y, model):
    x_train, x_test, y_train, y_test = train_test_split(X, Y)
    standard_scaler = StandardScaler()
    standard_scaler.fit(x_train)
    x_train = standard_scaler.transform(x_train)
    x_test = standard_scaler.transform(x_test)
    model.standard_scaler = standard_scaler

    return x_train, x_test, y_train, y_test



