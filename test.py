import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from sklearn.model_selection import train_test_split
from datetime import datetime, timedelta
from common import load_data, predict_accuracy
from models import EnsemableLearning


def draw_test_predict_img(y_test, y_predict):
    x_label_data = np.arange(1, len(y_test) + 1, 1)
    plt.figure(1)
    plt.plot(x_label_data, y_test, 'r', label='test_data')
    plt.plot(x_label_data, y_predict, 'b', label='predict_data')
    plt.title('Accuracy: {:.2f}%'.format(predict_accuracy(y_test, y_predict)*100))
    plt.legend()

def draw_predict_days_img(x_date, y_data, new_x_date, predict_y_data):
    plt.figure(2)
    plt.plot(x_date, y_data, 'b', label='history')
    plt.plot(new_x_date, predict_y_data, 'r', label='predict')
    plt.title('Predict gold average price image')
    plt.legend()
    plt.show()


def predict_days(model, last_x_data, x_date, predict_days=10):
    predict_record = []
    new_x_date = []
    last_date = datetime.strptime(str(x_date[-1]).split()[0], '%Y-%m-%d')

    for i in range(predict_days):
        predict_value = model.predict(last_x_data.reshape(1, -1), standard_flag=False)
        predict_record.append(predict_value[0][0])
        last_x_data = np.append(last_x_data[1:], predict_value)
        new_x_date.append(pd.to_datetime(str(last_date + timedelta(days=1))))
        last_date = new_x_date[-1]

    return predict_record, new_x_date

if __name__ == '__main__':

    num_steps = 40
    data = load_data(num_steps)
    model = EnsemableLearning(data)
    model.generate_train_model(sub_model_num=10)

    accuracy = model.accuracy()
    print('误差范围内准确率: {:.2f}%'.format(accuracy*100))

    x_train, x_test, y_train, y_test = train_test_split(data['X'], data['Y'])
    y_predict = model.predict(x_test, standard_flag=False)

    draw_test_predict_img(np.reshape(y_test, (-1, 1)), np.reshape(y_predict, (-1, 1)))

    x_date = data['x_date']
    y_data = data['y_data']
    last_x_data = data['last_x_data']
    predict_record, new_x_date = predict_days(model, last_x_data, x_date, 15)
    for i, predict_price in zip(range(len(predict_record)), predict_record):
        print ('预测未来第 {} 天的平均价格为：{:.2f}'.format(i+1, predict_price))

    predict_record.insert(0, y_data[-1])
    new_x_date.insert(0, x_date[-1])
    draw_predict_days_img(x_date, y_data, new_x_date, predict_record)














