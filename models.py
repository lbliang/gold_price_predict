from sklearn.linear_model import SGDRegressor, Ridge
from sklearn.ensemble import RandomForestRegressor
from common import predict_accuracy, get_train_test_data
from BGDRegressor import BGDRegressor

import numpy as np


class EnsemableLearning:
    def __init__(self, data_package):
        self._X = data_package['X']
        self._Y = data_package['Y']

    def generate_train_model(self, sub_model_num=10, error_percent=0.01):
        self._models = []   # 存储不同种类的模型列表
        # 模型类列表，分别是随机森林，随机梯度下降回归，批量梯度下降回归，岭回归
        model_classes = [RandomForestRegressor, SGDRegressor, BGDRegressor, Ridge]
        self._models_num = sub_model_num * len(model_classes)# 计算所有模型的数量
        self._accuracy = 0  # 模型准确率
        for model_cls in model_classes:
            sub_models = [] # 子模型列表
            for _ in range(sub_model_num):
                # 每个种类的模型都构建一定数量的模型对象
                if model_cls is SGDRegressor:
                    sub_model = model_cls(n_iter=2000)
                elif model_cls is BGDRegressor:
                    sub_model = model_cls(n_iter=10000)
                else:
                    sub_model = model_cls()
                # 对数据进行预处理，分成训练数据集和测试数据集，同时对数据进行归一化处理
                x_train, x_test, y_train, y_test = get_train_test_data(self._X, self._Y, sub_model)
                sub_model.fit(x_train, y_train)# 对具体模型进行训练
                y_predicts = sub_model.predict(x_test)# 对测试数据集进行预测
                self._accuracy += predict_accuracy(y_test, y_predicts, error_percent)# 累算精确度
                sub_models.append(sub_model)
            self._models.append(sub_models)
        # 最后计算出集成模型的精确度
        self._accuracy /= self._models_num

    def accuracy(self):
        return self._accuracy

    def predict(self, x_test, standard_flag=True):
        # 让预测的结果维度与样本数相同
        y_predicts = np.zeros((1, x_test.shape[0]))
        # 所有模型进行预测
        for sub_models in self._models:
            for sub_model in sub_models:
                # 没有归一化的数据则用进行具体归一化
                if not standard_flag:
                    x_test_standard = sub_model.standard_scaler.transform(x_test)
                    y_predicts += sub_model.predict(x_test_standard)
                else:
                    y_predicts += sub_model.predict(x_test)
        # 返回所有预测值的均值
        return y_predicts / self._models_num
